using Foundation;
using System;
using UIKit;
using SinchSdk;

namespace SinchVerificationQs
{
    public partial class CodeEntryViewController : UIViewController
    {
        public CodeEntryViewController(IntPtr handle) : base(handle)
        {
        }

        public ISINVerificationProtocol Verification { get; set; }


        void Verify()
        {
            Verification.VerifyCode(txtCode.Text, (result, error) =>
            {
                if (result)
                {
                    lblStatus.Text = "Verification Successful";
                }
                else
                {
                    lblStatus.Text = "Verification Failed";
                    //TODO ShowError(error);
                }
            });
        }

        //#pragma mark -
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            txtCode.BecomeFirstResponder();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            //TODO InitAppearance();
        }
    }
}