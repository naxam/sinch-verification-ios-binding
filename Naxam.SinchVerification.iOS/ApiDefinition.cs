﻿using System;
using CoreFoundation;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace SinchSdk
{
    partial interface ISINVerificationProtocol {}
    partial interface ISINPhoneNumber {}
    partial interface ISINInitiationResult {}

	// typedef void (^SINLogCallback)(SINVLogSeverity, NSString * _Nonnull, NSString * _Nonnull, NSDate * _Nonnull);
	delegate void SINLogCallback (SINVLogSeverity arg0, string arg1, string arg2, NSDate arg3);

	// @protocol SINInitiationResult <NSObject>
	[Protocol, Model]
	[BaseType (typeof(NSObject))]
	interface SINInitiationResult
	{
		// @required @property (readonly, assign, nonatomic) BOOL success;
		[Abstract]
		[Export ("success")]
		bool Success { get; }

		// @required @property (readonly, nonatomic) NSString * contentLanguage;
		[Abstract]
		[Export ("contentLanguage")]
		string ContentLanguage { get; }
	}

	// @interface SINVerification : NSObject
	[BaseType (typeof(NSObject))]
	interface SINVerification
	{
		// +(id<SINVerification> _Nonnull)SMSVerificationWithApplicationKey:(NSString * _Nonnull)applicationKey phoneNumber:(NSString * _Nonnull)phoneNumber;
		[Static]
		[Export ("SMSVerificationWithApplicationKey:phoneNumber:")]
		ISINVerificationProtocol SMSVerificationWithApplicationKey (string applicationKey, string phoneNumber);

		// +(id<SINVerification> _Nonnull)SMSVerificationWithApplicationKey:(NSString * _Nonnull)applicationKey phoneNumber:(NSString * _Nonnull)phoneNumber languages:(NSArray<NSString *> * _Nonnull)languageTags;
		[Static]
		[Export ("SMSVerificationWithApplicationKey:phoneNumber:languages:")]
		ISINVerificationProtocol SMSVerificationWithApplicationKey (string applicationKey, string phoneNumber, string[] languageTags);

		// +(id<SINVerification> _Nonnull)SMSVerificationWithApplicationKey:(NSString * _Nonnull)applicationKey phoneNumber:(NSString * _Nonnull)phoneNumber custom:(NSString * _Nonnull)custom;
		[Static]
		[Export ("SMSVerificationWithApplicationKey:phoneNumber:custom:")]
		ISINVerificationProtocol SMSVerificationWithApplicationKey (string applicationKey, string phoneNumber, string custom);

		// +(id<SINVerification> _Nonnull)SMSVerificationWithApplicationKey:(NSString * _Nonnull)applicationKey phoneNumber:(NSString * _Nonnull)phoneNumber custom:(NSString * _Nonnull)custom languages:(NSArray<NSString *> * _Nonnull)languageTags;
		[Static]
		[Export ("SMSVerificationWithApplicationKey:phoneNumber:custom:languages:")]
		ISINVerificationProtocol SMSVerificationWithApplicationKey (string applicationKey, string phoneNumber, string custom, string[] languageTags);

		// +(id<SINVerification> _Nonnull)calloutVerificationWithApplicationKey:(NSString * _Nonnull)applicationKey phoneNumber:(NSString * _Nonnull)phoneNumber;
		[Static]
		[Export ("calloutVerificationWithApplicationKey:phoneNumber:")]
		ISINVerificationProtocol CalloutVerificationWithApplicationKey (string applicationKey, string phoneNumber);

		// +(id<SINVerification> _Nonnull)calloutVerificationWithApplicationKey:(NSString * _Nonnull)applicationKey phoneNumber:(NSString * _Nonnull)phoneNumber custom:(NSString * _Nonnull)custom;
		[Static]
		[Export ("calloutVerificationWithApplicationKey:phoneNumber:custom:")]
		ISINVerificationProtocol CalloutVerificationWithApplicationKey (string applicationKey, string phoneNumber, string custom);

		// +(NSString * _Nonnull)version;
		[Static]
		[Export ("version")]
		string Version { get; }

		// +(void)setLogCallback:(SINLogCallback _Nullable)block;
		[Static]
		[Export ("setLogCallback:")]
		void SetLogCallback ([NullAllowed] SINLogCallback block);
	}

    delegate void SINVerificatinoInitiateCompleted(ISINInitiationResult result, NSError error);
    delegate void SINVerificatinoVerifyCode(bool result, NSError error);

	// @protocol SINVerification <NSObject>
	[Protocol, Model]
	[BaseType (typeof(NSObject))]
	interface SINVerificationProtocol
	{
		// @required -(void)initiateWithCompletionHandler:(void (^ _Nonnull)(id<SINInitiationResult> _Nonnull, NSError * _Nullable))completionHandler;
		[Abstract]
		[Export ("initiateWithCompletionHandler:")]
		void InitiateWithCompletionHandler (SINVerificatinoInitiateCompleted completionHandler);

		// @required -(void)verifyCode:(NSString * _Nonnull)code completionHandler:(void (^ _Nonnull)(BOOL, NSError * _Nullable))completionHandler;
		[Abstract]
		[Export ("verifyCode:completionHandler:")]
		void VerifyCode (string code, SINVerificatinoVerifyCode completionHandler);

		// @required -(void)cancel;
		[Abstract]
		[Export ("cancel")]
		void Cancel ();

		// @required -(void)setEnvironmentHost:(NSString * _Nonnull)environmentHost;
		[Abstract]
		[Export ("setEnvironmentHost:")]
		void SetEnvironmentHost (string environmentHost);

		// @required -(void)setCompletionQueue:(dispatch_queue_t _Nonnull)completionQueue;
		[Abstract]
		[Export ("setCompletionQueue:")]
		void SetCompletionQueue (DispatchQueue completionQueue);
	}

	[Static]
	partial interface SINVerificationConstants
	{
		// extern NSString *const _Nonnull SINVerificationDidBeginInitiatingNotification;
		[Field ("SINVerificationDidBeginInitiatingNotification", "__Internal")]
		NSString DidBeginInitiatingNotification { get; }

		// extern NSString *const _Nonnull SINVerificationDidEndInitiatingNotification;
		[Field ("SINVerificationDidEndInitiatingNotification", "__Internal")]
		NSString DidEndInitiatingNotification { get; }

		// extern NSString *const _Nonnull SINVerificationDidBeginVerifyingCodeNotification;
		[Field ("SINVerificationDidBeginVerifyingCodeNotification", "__Internal")]
		NSString DidBeginVerifyingCodeNotification { get; }

		// extern NSString *const _Nonnull SINVerificationDidEndVerifyingCodeNotification;
		[Field ("SINVerificationDidEndVerifyingCodeNotification", "__Internal")]
		NSString DidEndVerifyingCodeNotification { get; }

		// extern NSString *const _Nonnull SINVerificationDidBeginVerifyingCalloutNotification;
		[Field ("SINVerificationDidBeginVerifyingCalloutNotification", "__Internal")]
		NSString DidBeginVerifyingCalloutNotification { get; }

		// extern NSString *const _Nonnull SINVerificationDidEndVerifyingCalloutNotification;
		[Field ("SINVerificationDidEndVerifyingCalloutNotification", "__Internal")]
		NSString DidEndVerifyingCalloutNotification { get; }

		// extern NSString *const _Nonnull SINVerificationErrorDomain;
		[Field ("SINVerificationErrorDomain", "__Internal")]
		NSString ErrorDomain { get; }

		// extern NSString *const _Nonnull SINServiceErrorReferenceKey;
		[Field ("SINServiceErrorReferenceKey", "__Internal")]
		NSString SErrorReferenceKey { get; }
	}

	// @protocol SINPhoneNumber <NSObject, NSCopying>
	[Protocol, Model]
	[BaseType (typeof(NSObject))]
	interface SINPhoneNumber : INSCopying
	{
		// @required @property (readonly, nonatomic, strong) NSNumber * _Nonnull countryCode;
		[Abstract]
		[Export ("countryCode", ArgumentSemantic.Strong)]
		NSNumber CountryCode { get; }

		// @required @property (readonly, nonatomic, strong) NSString * _Nullable rawInput;
		[Abstract]
		[NullAllowed, Export ("rawInput", ArgumentSemantic.Strong)]
		string RawInput { get; }
	}

	[Static]
	partial interface SINPhoneNumberConstants
	{
		// extern NSString *const _Nonnull SINPhoneNumberParseErrorDomain __attribute__((visibility("default")));
		[Field ("SINPhoneNumberParseErrorDomain", "__Internal")]
		NSString ParseErrorDomain { get; }

		// extern NSString *const _Nonnull SINPhoneNumberValidationErrorDomain __attribute__((visibility("default")));
		[Field ("SINPhoneNumberValidationErrorDomain", "__Internal")]
		NSString ValidationErrorDomain { get; }

		// extern NSString *const _Nonnull SINPhoneNumberRawInputKey __attribute__((visibility("default")));
		[Field ("SINPhoneNumberRawInputKey", "__Internal")]
		NSString RawInputKey { get; }

		// extern NSString *const _Nonnull SINPhoneNumberCountryCodeKey __attribute__((visibility("default")));
		[Field ("SINPhoneNumberCountryCodeKey", "__Internal")]
		NSString CountryCodeKey { get; }
	}

	// @protocol SINPhoneNumberUtil <NSObject>
	[Protocol, Model]
	[BaseType (typeof(NSObject))]
	interface SINPhoneNumberUtil
	{
		// @required -(id<SINPhoneNumber> _Nullable)parse:(NSString * _Nonnull)string defaultRegion:(NSString * _Nonnull)isoCountryCode error:(NSError * _Nullable * _Nullable)error;
		[Abstract]
		[Export ("parse:defaultRegion:error:")]
		[return: NullAllowed]
		ISINPhoneNumber Parse (string @string, string isoCountryCode, [NullAllowed] out NSError error);

		// @required -(NSString * _Nonnull)formatNumber:(id<SINPhoneNumber> _Nonnull)phoneNumber format:(SINPhoneNumberFormat)format;
		[Abstract]
		[Export ("formatNumber:format:")]
		string FormatNumber (ISINPhoneNumber phoneNumber, SINPhoneNumberFormat format);

		// @required -(id<SINPhoneNumber> _Nullable)exampleNumberForRegion:(NSString * _Nonnull)isoCountryCode;
		[Abstract]
		[Export ("exampleNumberForRegion:")]
		[return: NullAllowed]
		ISINPhoneNumber ExampleNumberForRegion (string isoCountryCode);

		// @required -(BOOL)isPossibleNumber:(NSString * _Nonnull)string fromRegion:(NSString * _Nonnull)isoCountryCode error:(NSError * _Nullable * _Nullable)error;
		[Abstract]
		[Export ("isPossibleNumber:fromRegion:error:")]
		bool IsPossibleNumber (string @string, string isoCountryCode, [NullAllowed] out NSError error);

		// @required -(BOOL)isPossibleNumber:(id<SINPhoneNumber> _Nonnull)phoneNumber error:(NSError * _Nullable * _Nullable)error;
		[Abstract]
		[Export ("isPossibleNumber:error:")]
		bool IsPossibleNumber (ISINPhoneNumber phoneNumber, [NullAllowed] out NSError error);

		// @required -(NSNumber * _Nullable)countryCallingCodeForRegion:(NSString * _Nonnull)isoCountryCode;
		[Abstract]
		[Export ("countryCallingCodeForRegion:")]
		[return: NullAllowed]
		NSNumber CountryCallingCodeForRegion (string isoCountryCode);

		// @required -(id<SINRegionList> _Nonnull)regionListWithLocale:(NSLocale * _Nonnull)locale;
		[Abstract]
		[Export ("regionListWithLocale:")]
		SINRegionList RegionListWithLocale (NSLocale locale);
	}

	// @interface SINDeviceRegion : NSObject
	[BaseType (typeof(NSObject))]
	interface SINDeviceRegion
	{
		// +(NSString * _Nonnull)currentCountryCode;
		[Static]
		[Export ("currentCountryCode")]
		string CurrentCountryCode { get; }
	}

    delegate void TextDidChange(UITextField textField);

	// @interface SINUITextFieldPhoneNumberFormatter : NSObject
	[BaseType (typeof(NSObject))]
	interface SINUITextFieldPhoneNumberFormatter
	{
		// @property (readwrite, copy, nonatomic) NSString * _Nonnull countryCode;
		[Export ("countryCode")]
		string CountryCode { get; set; }

		// @property (readwrite, nonatomic, strong) UITextField * _Nullable textField;
		[NullAllowed, Export ("textField", ArgumentSemantic.Strong)]
		UITextField TextField { get; set; }

		// @property (readwrite, copy, nonatomic) void (^ _Nullable)(UITextField * _Nonnull) onTextFieldTextDidChange;
		[NullAllowed, Export ("onTextFieldTextDidChange", ArgumentSemantic.Copy)]
		TextDidChange OnTextFieldTextDidChange { get; set; }

		// -(instancetype _Nonnull)initWithCountryCode:(NSString * _Nonnull)isoCountryCode;
		[Export ("initWithCountryCode:")]
		IntPtr Constructor (string isoCountryCode);

		// -(instancetype _Nonnull)initWithPhoneNumberUtil:(id<SINPhoneNumberUtil> _Nonnull)phoneNumberUtil countryCode:(NSString * _Nonnull)isoCountryCode;
		[Export ("initWithPhoneNumberUtil:countryCode:")]
		IntPtr Constructor (SINPhoneNumberUtil phoneNumberUtil, string isoCountryCode);

		// -(NSString * _Nonnull)exampleNumberWithFormat:(SINPhoneNumberFormat)format;
		[Export ("exampleNumberWithFormat:")]
		string ExampleNumberWithFormat (SINPhoneNumberFormat format);
	}

	// @protocol SINRegionInfo <NSObject>
	[Protocol, Model]
	[BaseType (typeof(NSObject))]
	interface SINRegionInfo
	{
		// @required @property (readonly, nonatomic, strong) NSString * _Nonnull isoCountryCode;
		[Abstract]
		[Export ("isoCountryCode", ArgumentSemantic.Strong)]
		string IsoCountryCode { get; }

		// @required @property (readonly, nonatomic, strong) NSString * _Nonnull countryDisplayName;
		[Abstract]
		[Export ("countryDisplayName", ArgumentSemantic.Strong)]
		string CountryDisplayName { get; }

		// @required @property (readonly, nonatomic, strong) NSNumber * _Nonnull countryCallingCode;
		[Abstract]
		[Export ("countryCallingCode", ArgumentSemantic.Strong)]
		NSNumber CountryCallingCode { get; }
	}

	// @protocol SINRegionList <NSObject>
	[Protocol, Model]
	[BaseType (typeof(NSObject))]
	interface SINRegionList
	{
		// @required @property (readonly, nonatomic, strong) NSArray<id<SINRegionInfo>> * _Nonnull entries;
		[Abstract]
		[Export ("entries", ArgumentSemantic.Strong)]
		SINRegionInfo[] Entries { get; }

		// @required -(NSString * _Nonnull)displayNameForRegion:(NSString * _Nonnull)isoCountryCode;
		[Abstract]
		[Export ("displayNameForRegion:")]
		string DisplayNameForRegion (string isoCountryCode);

		// @required -(NSNumber * _Nullable)countryCallingCodeForRegion:(NSString * _Nonnull)isoCountryCode;
		[Abstract]
		[Export ("countryCallingCodeForRegion:")]
		[return: NullAllowed]
		NSNumber CountryCallingCodeForRegion (string isoCountryCode);
	}
}
