using System;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace SinchSdk
{
	[Native]
	public enum SINVLogSeverity : nint
	{
		Trace = 0,
		Info,
		Warning,
		Critical
	}

	[Verify (InferredFromMemberPrefix)]
	public enum SINVerificationError : uint
	{
		InvalidInput = 1,
		IncorrectCode,
		CalloutFailure,
		Timeout,
		Cancelled,
		ServiceError
	}

	[Native]
	public enum SINPhoneNumberFormat : nint
	{
		E164,
		International,
		National
	}

	[Native]
	public enum SINPhoneNumberParseError : nint
	{
		InvalidCountryCode,
		NotANumber,
		TooShortAfterIDD,
		TooShortNSN,
		TooLongNSN
	}

	[Native]
	public enum SINPhoneNumberValidationError : nint
	{
		InvalidCountryCode,
		NotANumber,
		TooShort,
		TooLong,
		InvalidNumber
	}

	static class CFunctions
	{
		// extern id<SINPhoneNumberUtil> _Nonnull SINPhoneNumberUtil () __attribute__((visibility("default")));
		[DllImport ("__Internal")]
		[Verify (PlatformInvoke)]
		static extern SINPhoneNumberUtil SINPhoneNumberUtil ();

		// extern id<SINPhoneNumberUtil> _Nonnull SINPhoneNumberUtilCreate () __attribute__((visibility("default")));
		[DllImport ("__Internal")]
		[Verify (PlatformInvoke)]
		static extern SINPhoneNumberUtil SINPhoneNumberUtilCreate ();
	}
}
